/*!
 * Copyright (c) 2023 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Olivier Goulet <olivier.goulet@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import DataTable from 'datatables.net';
import getAllCVEs from './api/eclipsefdn.cve';


const KnownVulnerabilitiesTable = async () => {
    const element = document.querySelector('#known-table-wrapper');
    if (!element) return;

    const [cves, error] = await getAllCVEs();

    if (error) {
        element.innerHTML = `
            <p class="alert alert-danger">
                An error has occurred while fetching known vulnerabilities. Please try again later.
            </p>
        `;

        return;
    }

    const data = cves.filter(row => row.cvePullRequest !== null);

    populateTable(data);
}

export default KnownVulnerabilitiesTable();

/** Populate the known vulnerabilities table with CVE data
  * @param {Array} data - Array of CVEs
  * @returns {DataTable} - DataTable instance
*/
const populateTable = data =>   
    new DataTable('#known-table', {
        data,
        autoWidth: false,
        columns: [
            { 
                data: 'id',
                width: '20%',
                render: (data, _, row) => `<a href="${row.nvdLink}">${data}</a>`
            },
            { data: 'datePublished', width: '15%' },
            { 
                name: 'project',
                data: 'project', 
                width: '25%',
                render: (data) => `<a class="known-table-project-link" href="https://projects.eclipse.org/projects/${data}">${data}</a>` 
            },
            //{ data: 'summary' }
        ],
        order: [[1, 'desc']],
        pageLength: 10,
        lengthMenu: [10, 20, 50, 100]
    }
);
