// Fits more projects per row. Temporary solution to make the projects take up less vertical space.
// Should remove once we have pagination.
const ResearchProjectsSection = () => {
    const projectListElem = document.getElementById('projects-list');
    if (projectListElem) {
        projectListElem.className = 'col-md-24'
    }

    return;
};

export default new ResearchProjectsSection();