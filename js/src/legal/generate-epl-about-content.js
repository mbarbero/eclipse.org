/**
 * Copyright (c) 2024 Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import getBodyContent from "./get-body-content";

const generateEplAboutTemplates = async () => {
    document.getElementById('epl-2-about-content').innerHTML = await getBodyContent('/legal/epl/about-file-templates/epl-2-about.html');
    document.getElementById('epl-1-about-content').innerHTML = await getBodyContent('/legal/epl/about-file-templates/epl-1-about.html');
    document.getElementById('epl-2-about-long-content').innerHTML = await getBodyContent('/legal/epl/about-file-templates/epl-2-longabout.html');
    document.getElementById('epl-1-about-long-content').innerHTML = await getBodyContent('/legal/epl/about-file-templates/epl-1-longabout.html');
}

export default generateEplAboutTemplates();
