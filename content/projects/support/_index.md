---
title:  Project Support
hide_sidebar: true
keywords: ["Helpdesk", "Eclipse IDE support", "eclipse project help", "troubleshooting"]
description: Get the help you need with Eclipse Projects
---

Eclipse Projects are created and managed by our community of developers. If you need support with any of our open source projects, you should:

1. [Find the project page](https://projects.eclipse.org/)
2. Navigate to the “Developer Resources” section of the page and find the project repositories. Open a ticket or PR directly against the project of interest.
3. If you are still having trouble, reach out to project lead(s) or committers involved in the project - found on the “Who’s involved” page.

If you are unable to resolve your issue with the project, create an issue on the [Eclipse Foundation GitLab helpdesk](https://gitlab.eclipse.org/eclipsefdn/helpdesk) or [contact us](/org/foundation/contact.php).

For issues related to security, find out how to [report a vulnerability](/security) and privacy concerns can be sent to [privacy@eclipse.org](mailto:privacy@eclipse.org).