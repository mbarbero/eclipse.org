---
title: "Committer Members"
seo_title: "Become a Committer Member"
description: >-
  Committer Members are individuals who are the core developers of the Eclipse
  projects and can commit changes to project source code. Find out how you can
  become a Committer Member of the Eclipse Foundation.
keywords: ["committer", "membership", "become a committer", "eclipse committer", "eclipse project committer"]
---

Before you can become a Committer Member of the Eclipse Foundation, you must
first become a Committer. While these things may sound similar, they are
actually quite different.

- **Committers** are those people who have write access to the source
  repositories and other content on the Eclipse Foundation's website. A good
  description can be found in the [Eclipse Membership Agreement](/org/documents/eclipse-foundation-membership-agreement.pdf):
  
  > "In order for an individual to become a Committer, another Committer for
  > the project (or component) must nominate that individual. Once an
  > individual is nominated, the existing Committers for the project (or
  > component) will vote using the process and rules established in the
  > project’s corresponding Charter. When a new project is started, the
  > corresponding PMC will nominate an initial set of Committers for approval
  > by the Executive Director (or his delegates). Becoming a Committer is a
  > privilege that is earned by contributing and showing discipline and good
  > judgment. It is a responsibility that should be neither given nor taken
  > lightly."
  
  Please see [Step 1](#becoming-a-committer) below for more details. 
- **Committer Members** are those Committers who decide to join the Eclipse
  Foundation as Members. Note that people who work for a Member company who are
  Committers do not need to join as individual Committer Members. They already
  have all of the rights and privileges of a Committer Member. It is also
  important to understand that you do not have to join the Foundation as a
  Committer Member to be a Committer.

  One of the important benefits of becoming a Committer Member is that you are
  eligible to vote in the elections for the Committer Representatives on the
  Eclipse Board of Directors.

## Step 1 &mdash; Becoming a Committer

If you are interested in becoming a Committer at Eclipse, the first and most
important question is which project do you want to contribute to?

- There are many [projects](https://projects.eclipse.org); you should start
  with the project that is the most interesting and most exciting to you.

Second, a few operational details:

- Please note that the [forums](/forums/) are a good place to make contact with
  a project. To request a Eclipse Foundation password to post on the forums,
  report bugs and enter feature requests, please go to our 
  [account creation form](https://accounts.eclipse.org/user/register).
- We distinguish between two similar sounding, and related, organizations: the
  Eclipse Foundation is a member-funded not-for-profit corporation that
  supports the Eclipse Open Source Projects. The Projects are the open source
  community where the development happens. This distinction is important
  because you can be an active developer and contributor to the open source
  Projects without being a "member" of the Eclipse Foundation. (Most of our
  non-affiliated open source committers are also members of the Foundation, but
  it is not a requirement.) There is no cost to being an individual member of
  the Foundation.

Third, about becoming a developer and committer on the Eclipse Open Projects:

- Becoming a Committer at Eclipse is a multi-stage process. The basic rule is
  that all new committers are voted in by the existing committers on a project.
  Similar to other open source communities, we operate as a
  meritocracy....which among other things translates to mean that you have to
  prove your abilities before you can become a committer.
- The best way to start contributing to Eclipse is to start with well-formed
  bug reports and feature requests for project(s) that you are primarily
  interested in. Build a good reputation with the existing committers on a
  project. Over time, start proposing code enhancements and volunteer time to
  the project. Then once you have demonstrated your skills to the existing
  team, ask a current project committer to sponsor you as a committer. The
  election process is defined in each top-level project's charter.
  
## Step 2 &mdash; Becoming a Committer Member of the Eclipse Foundation

Once you are a Committer on one or more Eclipse projects, you will
automatically be considered a Committer Member if you are employed by a Member
organization of the Foundation. That is, if your company is already a Member,
then you are automatically a Committer Member.

You may also choose to join as a Committer Member even if you are not employed
by a Member company. To do so, kindly contact <emo@eclipse.org> and ask them to
send you the Membership Agreement for electronic signature.

Once you become a Committer Member, you are eligible to participate in the
Eclipse Foundation’s General Assembly, to vote for Committer representatives on
the board of directors, etc. A full list of the benefits are available 
[here](/membership/#tab-levels).
