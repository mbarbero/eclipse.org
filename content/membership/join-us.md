---
title: "Contact us about your organisation joining the Eclipse Foundation"
keywords: ["Eclipse Membership", "Eclipse Members", "Become an Eclipse Member"]
hide_sidebar: true
container: container padding-bottom-30
---

{{< grid/div class="row padding-top-30" isMarkdown="false" >}}
{{< grid/div class="col-sm-7" >}}
If you are looking for more information on all of the benefits of Eclipse
Foundation memberships, download the [Membership Prospectus](/membership/documents/membership-prospectus.pdf).

Looking for information on joining as a Committer Member of the Eclipse
Foundation? [Check this out](/membership/become-a-member/committer)!
{{</ grid/div >}}
{{< grid/div class="col-sm-16 col-sm-offset-1" isMarkdown="false" >}}
  {{< grid/div class="solstice-block-default solstice-block-white-bg" isMarkdown="false" >}}
    {{< hubspot_contact_form portalId="5413615" formId="3fe3f2f6-1dbb-45f5-83e7-a219dc2e83f9" >}} 
  {{</ grid/div >}}
{{</ grid/div >}}
{{</ grid/div >}}
