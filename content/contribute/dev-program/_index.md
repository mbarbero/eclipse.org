---
title: "Friend of Eclipse Enhancement Program (FEEP)"
date: 2022-07-27T10:00:00-04:00
description: "Contribute to Eclipse Foundation"
categories: []
keywords: ["eclipse", "foundation", "development", "platform", "funding"]
slug: "" 
aliases: []
toc: false
draft: false
container: container
layout: single
is_deprecated: true
---

{{< pages/contribute/dev-program >}}
