---
title: FAQ
date: 2022-07-27T10:00:00-04:00
description: "Contribute to Eclipse Foundation"
seo_title: FEEP Bidding Process FAQ
categories: []
keywords: ["eclipse", "foundation", "development", "platform", "funding", "faq", "feep"]
slug: "" 
aliases: []
toc: false
draft: false
hide_page_title: true
container: container
is_deprecated: true
---

# FEEP Bidding Process FAQ

## How can I submit a bid for a development effort?

Click on the link for any one of the [development efforts](https://projects.eclipse.org/development-efforts), and you will see a button allowing you to generate and submit a bid. Please note that you must be logged in to do so.

## Must I be a committer to submit a bid? Must my company be a member to submit a bid?

Technically, no – this is not mandatory. However, strong preference will be given to bids
submitted by member companies and individual committers.

## Can I propose an alternative solution to what is being requested in the description of the Development Effort?

Yes. It is our goal to ensure we have the best possible outcome from these Development Efforts,
and if you feel we should consider an alternative approach, please feel free to propose it. You
will, however, have to make clear the impact to scope, and please recognize if the cost you are
proposing is greater than what was originally allocated, the proposed solution may not be
feasible.

## Can I get clarification on a specific Development Effort?

Yes. Please send email to [feep@eclipse.org](mailto:feep@eclipse.org). Please be aware
that we may post updates to the specific Development Efforts based on the questions we receive.

## Must my bid be a fixed price bid?

No, you may propose an alternative pricing mechanism such as time and materials, but be aware
that preference will be given to fixed price bids. If you wish to specify an alternative to fixed
price, please indicate this clearly in your bid.

## Can I bid on multiple development efforts?

Yes, but please submit each bid as a separate proposal. Each development effort will be managed
through a separate Statement of Work (SOW).

## How should I indicate the impact to my delivery schedule if I am awarded multiple Development Efforts?

If you are bidding on more than one development effort, please make clear the impact of the
deliverable schedule should you be awarded multiple SOWs. This detail should be included in each
bid impacted.

## How are bids qualified? Who is making the final decision regarding bids?

All bids will be evaluated by the FEEP-PM in consultation with the team that helped develop the
original list of Development Efforts. Bids will be evaluated using the criteria specified in the
FEEP Program Description.

## When will I know whether my bid has won, and when can I begin working on the Development Effort?

The FEEP-PM will contact you directly should your bid(s) be selected as a winning bid. You will
be required to execute a services contract with Eclipse Foundation, if you have not already done
so, and then to execute a work schedule to contract for the specific Development Effort.
