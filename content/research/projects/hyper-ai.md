---
title: "HYPER-AI"
date: 2024-04-01T00:00:00-00:00
project_logo: "/research/images/research/r-projects/hyper-ai.png"
tags: ["Information science", "Bioinformatics", "Computer hardware", "Architecture", "Artificial Intelligence", "Asynchronous Multi-Agent Optimization", "Reinforcement Learning", "Semantic Context", "Open Ontological Representations", "Autonomous Swarming", "Distributed-Ledger", "Privacy and Security"]
homepage: "https://hyper-ai-project.eu/"
facebook: ""
linkedin: "https://www.linkedin.com/company/hyper-ai-project/"
twitter: "https://twitter.com/HYPERAI_Project"
youtube: "https://www.youtube.com/channel/UCm8tmjbeFL_feSMbD3Ohd6g/"
funding_bodies: ["horizonEU"]
eclipse_projects: []
project_topic: "Cloud-Edge, IoT"
summary: "Hyper-Distributed AI Platform for Network Resources Automation and Management Towards More Efficient Data Processing Applications"
---

In HYPER-AI, we work with smart virtual computing entities (nodes) that come from a variety of infrastructures that span all three of
the so-called computing continuum's layers: the Cloud, the Edge, and IoT.

It focuses on intensive data-processing applications that present the potential to improve their footprint when hyper-distributed in
an optimized manner. In order to give targeted applications access to computational, storage, or network services, HYPER-AI
implements the idea of computing swarms as autonomous, self-organized, and opportunistic networks of smart nodes. These
networks may offer a diverse and heterogeneous set of resources processing, storage, data, communication) at all levels and have the
ability to dynamically connect, interact, and cooperate.

HYPER-AI proposes semantic representation concepts to enable heterogeneous resources’ abstraction in a homogeneous way, under
a common annotation (computing node), across the whole range of network infrastructures. The main orchestration and control
concept of HYPER-AI is inspired by autonomic systems (self-CHOP principles) which employ swarmed computing schemes. Its
objective is to make smart multi-node (swarm) deployment scenario design, execution, and monitoring easier, through appropriate
AIs for self-configuration (nodes assigned resources), self-healing (swarmed nodes lifecycle), self-optimizing (exploiting built-in
situation awareness mechanisms) and self-protecting (intrusion detection, privacy, security, encryption and identity management) at
application runtime. In order to support dynamic and data-driven application workflows, HYPER-AI suggests the flexible integration of
resources at the edge, the core cloud, and along the big data processing and communication channel, enabling their energy, time
and cost-efficient execution. Finally, distributed ledger concepts for security, privacy, and encryption as well as AI-based intrusion
detection are also considered.


This project is running from April 2024 - March 2027.

---

## Consortium
* ETHNIKO KENTRO EREVNAS KAI TECHNOLOGIKIS ANAPTY (Coordinator) - Greece
* TELEFONICA INVESTIGACION Y DESARROLLO SA - Spain 
* ECLIPSE FOUNDATION EUROPE GMBH - Germany
* ETHNIKO KAI KAPODISTRIAKO PANEPISTIMIO ATHINON - Greece
* VIRTUAL VEHICLE RESEARCH GMBH - Austria
* ODIN SOLUTIONS SOCIEDAD LIMITADA - Spain
* SABO SA - Greece
* TRUST-IT SRL - Italy
* COMMPLA SRL (Affiliated) - Italy
* TECHNOLOGIKO PANEPISTIMIO KYPROU - Cyprus 
* EBOS TECHNOLOGIES LIMITED - Cyprus
* AGENZIA NAZIONALE PER LE NUOVE TECNOLOGIE- Italy
* CSEM CENTRE SUISSE D'ELECTRONIQUE ET DE MICROTECH (Associated) - Switzerland 
* HAUTE ECOLE SPECIALISEE DE SUISSE OCCIDENTALE (Associated) - Switzerland 
* SUNDOSOFT LTD (Associated) - Korea
