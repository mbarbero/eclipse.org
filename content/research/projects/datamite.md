---
title: "DATAMITE"
date: 2023-01-01T00:00:00-00:00
project_logo: "/research/images/research/r-projects/datamite.png"
tags: ["Interoperability", "Data Monetization", "Data Exploitation", "Data Quality", "Data Governance", "Data Spaces", "EU AI-on-demand platform", "EOSC", "Data Sovereignty"]
homepage: "https://datamite-horizon.eu/"
facebook: ""
linkedin: "https://www.linkedin.com/company/datamite"
twitter: "https://twitter.com/DATAMITE_EU"
youtube: ""
zenodo:  ""
funding_bodies: ["horizonEU"]
eclipse_projects: []
project_topic: "Data Space"
summary: "DATA Monetization, Interoperability, Trading & Exchange"
---

DATAMITE empowers European companies by delivering a modular, open-source and multi-domain Framework to improve DATA
Monetizing, Interoperability, Trading and Exchange, in the form of software modules, training, and business materials.
DATAMITE unleashes the monetization potential at two levels. At internal level, users will have tools to improve quality management
of their data, the adherence to FAIR principles, and will be able to upskill on technical and business aspects thanks to the multiple
open-source training materials the project will generate. Therefore, data will become trustable and more reliable also in other
paradigms like AI.

At external level, keeping users in control of their data will provide new sources of revenue and interaction with other stakeholders.
The architecture envisioned for DATAMITE enables DIHs sandboxing, becoming a potential instructor on their onboarding of SMEs
and low-tech SMEs into the data economy. Together, DATAMITE’s solutions will function as a catalyst to boost data monetization in
the European productive fabric.

DATAMITE will validate the results in 3 different use cases with a total of 6 pilots, demonstrating that the Framework is interoperable
and usable in different domains and user needs, as: 1) Intra-corporate, multi-domain data exchange; 2) Data trading among Data
Spaces; 3) Integration with other initiatives as Data Markets, EU AI-on-demand platform, or DIHs. Sectors covered by the pilots are:
agriculture, energy, industrial and manufacturing, and climate.

To achieve this, the project relies on a consortium of 27 partners from 13 countries, bringing together key actors of the Data Value
Chain: Data Spaces technical and business stakeholders, multiple key communities (IDSA, Gaia-X, EUHUBS4DATA, AI4EUROPE, EOSC),
key experts in Legal and SSH aspects to guarantee legal and societal compliance, and facilitators on open-source community building
and standardisation activities to accelerate the transfer to the market.


This project is running from January 2023 - December 2025.

---

## Consortium
* Instituto Tecnológico de Informática (Coordinator) ES
* University College Cork IE
* Forschungsinstitut fuer Rationalisierung DE
* Gimeno Digital Technologies, S.L. ES
* Fraunhofer Gesellschaft für Angewandte Wissenschaften DE
* Instytut Chemii Bioorganicznej PAN PL
* Ethniko Kentro Erevnas Kai Technologikis Anaptyxis GR
* Institute of Communication and Computer Systems GR
* AUSTRALO Alpha Lab MTÜ	EE
* Fundación Tecnalia Research & Innovation ES
* Pravo I Internet Foundation BG
* Stichting EGI NL
* Wielkopolska Agricultural Advisory Center in Poznan PL
* Hellenic Telecommunications Organization S.A GR
* International Data Spaces Association e.V.	 DE
* ENGIE FR
* IDFS Spółka z ograniczoną osobowością PL
* Hellenic Electricity Distribution Network Operator S.A. GR
* DIN Deutsches Institut fuer Normung e.V. DE
* GLOBAZ SA PT
* Dyad Labs Ltd IE
* Eclipse Foundation DE
* Zentrum für Soziale Innovation	 AT
* Cineca	 IT