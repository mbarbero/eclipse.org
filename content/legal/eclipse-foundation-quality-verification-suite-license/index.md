---
title: Eclipse Foundation Quality Verification Suite License - v 1.0
author: Eclipse Foundation
keywords:
  - legal
  - documents
  - license
  - quality verification
---

{{< pages/legal/eclipse-foundation-quality-verification-suite-license >}}
