---
title: EPL-2.0 FAQ
author: Wayne Beaton
hide_page_title: true
keywords:
  - epl
  - epl-2.0
  - faq
  - legal
  - foundation
  - eclipse
  - eclipse public license
  - license
  - licenses
---

{{< pages/legal/inject-content >}}
