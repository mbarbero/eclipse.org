---
title: Eclipse Public License 2.0 (EPL)
layout: single
author: Wayne Beaton
hide_page_title: true
keywords:
  - epl
  - epl-2.0
  - legal
  - foundation
  - eclipse
  - eclipse public license
  - license
  - licenses
---

{{< pages/legal/inject-content >}}
